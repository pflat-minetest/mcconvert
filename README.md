# MCConvert

Simple tool to convert Minecraft player skins to Minetest format.

### Building

The project has the following dependencies:
- [cxxopts](https://github.com/jarro2783/cxxopts)
- [lodepng](https://github.com/lvandeve/lodepng)

This dependencies are already included, so there's no need to add them.

To build, use:
```sh
$ mkdir build
$ cd build
$ cmake ../
$ cmake --build . -j 2 --config Release
```

A Code::Blocks project file is also included.

### Usage:

To get the parameter list:
```sh
MCConvert --help
```

Example usage:
```sh
MCConvert -input input.png -output output.png --leftarm --rightleg
```

#### Parameter descriptions

Minecraft skins have distinct texture areas for left and right arms and legs. Minetest only has textures for arms and legs (both arms and both legs always look the same). The following swithes control which textures to use (left or right):

- leftarm - copy the Minecrft left arm texture to both arms of the Minetest texture. Otherwise copy the right arm texture.
- leftleg - copy the Minecrft left leg texture to both legs of the Minetest texture. Otherwise copy the right leg texture.

Minecraft skins have distinct torso/jacket, arms/sleeves and legs/pants areas. Minetest only has torso, arms and legs. It's necessary to indicate if the jacket, sleeves and pants should be copied. The following switches do so:

- jacket - Writes the Minecraft jacket texture over the Minecraft torso texture.
- sleeve - Writes the Minecraft sleeves texture over the Minecraft arms texture. The parameter leftarm is used to determine which sleeves texture to use.
- pants - Writes the Minecraft pants texture over the Minecraft legs texture. The parameter leftleg is used to determine which pants texture to use.

