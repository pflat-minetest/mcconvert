#pragma once

#include "color.hpp"

class Image
{
public:
    Image();

    bool create(unsigned int width, unsigned int height, const Color& color);
    bool loadFromFile(const std::string& filename);
    bool saveToFile(const std::string& filename);

    Color getPixel(unsigned int x, unsigned int y) const;
    void setPixel(unsigned int x, unsigned int y, const Color& color);

    void copy(const Image& img2,
              unsigned int x1, unsigned int y1,
              unsigned int x2, unsigned int y2,
              unsigned int width, unsigned int height);

private:
    std::vector<unsigned char> m_image; //!< The raw pixels
    unsigned int m_width;
    unsigned int m_height;
};
