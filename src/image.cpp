#include <iostream>
#include "lodepng.h"
#include "image.hpp"

////////////////////////////////////////////////////////////
Image::Image() :
    m_width(0),
    m_height(0)
{

}

////////////////////////////////////////////////////////////
bool Image::create(unsigned int width, unsigned int height, const Color& color)
{
    m_width = width;
    m_height = height;

    m_image.resize(width * height * 4);

    for(unsigned int y = 0; y < height; y++)
    {
        for(unsigned int x = 0; x < width; x++)
        {
            unsigned int index = y * width * 4 + x * 4;
            m_image[index + 0] = color.r;
            m_image[index + 1] = color.g;
            m_image[index + 2] = color.b;
            m_image[index + 3] = color.a;
        }
    }
    return true;
}

////////////////////////////////////////////////////////////
bool Image::loadFromFile(const std::string& filename)
{
    unsigned int error = lodepng::decode(m_image, m_width, m_height, filename);
    if (error == 0)
    {
        return true;
    }
    else
    {
        std::cout << "PNG decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
        return false;
    }
}

////////////////////////////////////////////////////////////
bool Image::saveToFile(const std::string& filename)
{
    std::vector<unsigned char> png;
    lodepng::State state; //optionally customize this one

    unsigned int error = lodepng::encode(png, m_image, m_width, m_height, state);
    if (!error)
    {
        lodepng::save_file(png, filename);
        return true;
    }
    else
    {
        std::cout << "PNG encoder error " << error << ": "<< lodepng_error_text(error) << std::endl;
        return false;
    }
}

////////////////////////////////////////////////////////////
Color Image::getPixel(unsigned int x, unsigned int y) const
{
    unsigned int index = y * m_width * 4 + x * 4;
    return Color(m_image[index + 0], m_image[index + 1], m_image[index + 2], m_image[index + 3]);
}

////////////////////////////////////////////////////////////
void Image::setPixel(unsigned int x, unsigned int y, const Color& color)
{
    unsigned int index = y * m_width * 4 + x * 4;
    m_image[index + 0] = color.r;
    m_image[index + 1] = color.g;
    m_image[index + 2] = color.b;
    m_image[index + 3] = color.a;
}

////////////////////////////////////////////////////////////
void Image::copy(const Image& img2,
                 unsigned int x1, unsigned int y1,
                 unsigned int x2, unsigned int y2,
                 unsigned int width, unsigned int height)
{
    for (unsigned int y = 0; y < height; y++)
    {
        for (unsigned int x = 0; x < width; x++)
        {
            Color c2 = img2.getPixel(x2 + x, y2 + y);
            if (c2.a > 0)
            {
/*
                Color c2 = getPixel(x2 + x, y2 + y);
                if (c2.a > 0)
                {
                    //cSrc.a = (unsigned int)((float)cSrc.a * alpha);

                    //dst.setPixel(dstX + x, dstY + y, AlphaBlendPixels(cSrc, cDst));

                    //unsigned int b = alphaBlend(cDst.toInteger(), cSrc.toInteger());

                    //unsigned int b = blendAlpha(cSrc.toInteger(), cDst.toInteger(), 128);

                    dst.setPixel(dstX + x, dstY + y, blend(cSrc, cDst, 255));
                }
                else
*/
                {
                    c2.a = 255;
                    setPixel(x1 + x, y1 + y, c2);
                }
            }
        }
    }
}
