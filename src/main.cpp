#include <iostream>
#include "cxxopts.hpp"
#include "image.hpp"


struct ConvertSettings
{
    std::string inputFile;
    std::string outputFile;
    bool useLeftArm;
    bool useLeftLeg;
    bool useJacket;
    bool useSleeve;
    bool usePants;
};


void copyArm(const Image& img1, Image& img2,
             unsigned int x1, unsigned int y1)
{
    img2.copy(img1, 40, 20, x1, y1 + 4, 4, 12);      //  arm right
    img2.copy(img1, 44, 20, x1 + 4, y1 + 4, 4, 12);  //  arm front

    img2.copy(img1, 48, 20, x1 + 7, y1 + 4, 4, 12);   //  arm left
    img2.copy(img1, 52, 20, x1 + 10, y1 + 4, 4, 12);  //  arm back

    img2.copy(img1, 40, 16, x1, y1, 4, 4);      //  shoulder above right arm
    img2.copy(img1, 44, 16, x1 + 4, y1, 2, 4);  //  shoulder above front arm
    img2.copy(img1, 46, 16, x1 + 5, y1, 2, 4);  //  shoulder above front arm
    img2.copy(img1, 48, 16, x1 + 7, y1, 2, 4);  //  shoulder above left arm
    img2.copy(img1, 50, 16, x1 + 8, y1, 4, 4);  //  shoulder above back arm
}



void parsecxx(int argc, const char* argv[], ConvertSettings& settings)
{
    try
    {
        cxxopts::Options options(argv[0], "Convert Minecraft skins to Minetest format.");

        options.add_options()
            ("h,help", "Help", cxxopts::value<bool>()->default_value("false")->implicit_value("true"))
            ("i,input", "Input file", cxxopts::value<std::string>())
            ("o,output", "Output file", cxxopts::value<std::string>())
            ("leftarm", "Use left arm", cxxopts::value<bool>()->default_value("false")->implicit_value("true"))
            ("leftleg", "Use left leg", cxxopts::value<bool>()->default_value("false")->implicit_value("true"))
            ("jacket", "Use jacket", cxxopts::value<bool>()->default_value("true")->implicit_value("true"))
            ("sleeve", "Use sleeve", cxxopts::value<bool>()->default_value("true")->implicit_value("true"))
            ("pants", "Use pants", cxxopts::value<bool>()->default_value("true")->implicit_value("true"));

        auto result = options.parse(argc, argv);

        if (result.count("help") > 0)
        {
          std::cout << options.help() << std::endl;
          exit(EXIT_SUCCESS);
        }

        if (result.count("input") == 0)
        {
          std::cout << "No input file specified." << std::endl;
          exit(EXIT_FAILURE);
        }

        if (result.count("output") == 0)
        {
          std::cout << "No output file specified." << std::endl;
          exit(EXIT_FAILURE);
        }

        settings.inputFile = result["input"].as<std::string>();
        settings.outputFile = result["output"].as<std::string>();
        settings.useLeftArm = result["leftarm"].as<bool>();
        settings.useLeftLeg = result["leftleg"].as<bool>();
        settings.useJacket = result["jacket"].as<bool>();
        settings.useSleeve = result["sleeve"].as<bool>();
        settings.usePants = result["pants"].as<bool>();

        std::cout << "Input file: " << settings.inputFile << std::endl;
        std::cout << "Output file: " << settings.outputFile << std::endl;
        std::cout << "Use left arm: " << settings.useLeftArm << std::endl;
        std::cout << "Use left leg: " << settings.useLeftLeg << std::endl;
        std::cout << "Use jacket: " << settings.useJacket << std::endl;
        std::cout << "Use sleeve: " << settings.useSleeve << std::endl;
        std::cout << "Use pants: " << settings.usePants << std::endl;
    }
    catch (const cxxopts::OptionException& e)
    {
        std::cout << "Error parsing options: " << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
}



int main(int argc, const char* argv[])
{
    //sf::Clock app_timer;

    ConvertSettings settings;
    parsecxx(argc, argv, settings);

    Image img1;
    std::cout << "Loading image: " << settings.inputFile << "\n";
    img1.loadFromFile(settings.inputFile);

    Image img2;
    img2.create(64, 32, Color::Transparent);

    img2.copy(img1, 0, 0, 0, 0, 32, 16);         //  head
    img2.copy(img1, 32, 0, 32, 0, 32, 16);       //  hat
    img2.copy(img1, 16, 16, 16, 16, 24, 16);     //  body
    if (!settings.useLeftLeg)
        img2.copy(img1, 0, 16, 0, 16, 16, 16);   //  right leg
    else
        img2.copy(img1, 0, 16, 16, 48, 16, 16);  //  left leg

    if (!settings.useLeftArm)
        copyArm(img1, img2, 40, 16);             //  right arm
    else
        copyArm(img1, img2, 32, 48);             //  left arm

    img2.copy(img1, 56, 16, 56, 16, 8, 16);      //  cape

    if (settings.useJacket)
        img2.copy(img1, 16, 16, 16, 32, 24, 16); //  jacket

    if (settings.usePants && !settings.useLeftLeg)
        img2.copy(img1, 0, 16, 0, 32, 16, 16);   //  right pants
    else if (settings.usePants && settings.useLeftLeg)
        img2.copy(img1, 0, 16, 0, 48, 16, 16);   //  left pants

    if (settings.useSleeve && !settings.useLeftArm)
        copyArm(img1, img2, 40, 32);             //  right sleeve
    else if (settings.useSleeve && settings.useLeftArm)
        copyArm(img1, img2, 48, 48);             //  left sleeve

    img2.saveToFile(settings.outputFile);

    //sf::Time app_elapsed = app_timer.restart();
    //std::cout << "Time elapsed : " << app_elapsed.asSeconds() << " s\n";

    return EXIT_SUCCESS;
}
