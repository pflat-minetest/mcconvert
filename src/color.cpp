#include "color.hpp"

const Color Color::Black(0, 0, 0);
const Color Color::White(255, 255, 255);
const Color Color::Red(255, 0, 0);
const Color Color::Green(0, 255, 0);
const Color Color::Blue(0, 0, 255);
const Color Color::Yellow(255, 255, 0);
const Color Color::Magenta(255, 0, 255);
const Color Color::Cyan(0, 255, 255);
const Color Color::Transparent(0, 0, 0, 0);

////////////////////////////////////////////////////////////
Color::Color() :
    r(0),
    g(0),
    b(0),
    a(255)
{

}

////////////////////////////////////////////////////////////
Color::Color(unsigned char red, unsigned char green, unsigned char blue, unsigned char alpha) :
    r(red),
    g(green),
    b(blue),
    a(alpha)
{

}
/*
////////////////////////////////////////////////////////////
Color::Color(unsigned int color) :
    r((color & 0xFF000000) >> 24),
    g((color & 0x00FF0000) >> 16),
    b((color & 0x0000FF00) >> 8),
    a(color & 0x000000FF)
{

}

////////////////////////////////////////////////////////////
unsigned int Color::toInteger() const
{
    return ((r << 24) | (g << 16) | (b << 8) | a);
}
*/
////////////////////////////////////////////////////////////
void Color::blend(const Color& color)
{
    r = (color.r * color.a + r * (255 - color.a)) / 255;
    g = (color.g * color.a + g * (255 - color.a)) / 255;
    b = (color.b * color.a + b * (255 - color.a)) / 255;
    //a = 255;
}
